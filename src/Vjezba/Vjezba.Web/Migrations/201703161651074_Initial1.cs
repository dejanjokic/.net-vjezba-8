namespace Vjezba.Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial1 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Companies", "CityID", "dbo.Cities");
            DropIndex("dbo.Companies", new[] { "CityID" });
            CreateTable(
                "dbo.CompanyContacts",
                c => new
                    {
                        value = c.String(nullable: false, maxLength: 128),
                        kontakt = c.Int(nullable: false),
                        Company_ID = c.Int(),
                    })
                .PrimaryKey(t => t.value)
                .ForeignKey("dbo.Companies", t => t.Company_ID)
                .Index(t => t.Company_ID);
            
            AlterColumn("dbo.Companies", "CityID", c => c.Int());
            CreateIndex("dbo.Companies", "CityID");
            AddForeignKey("dbo.Companies", "CityID", "dbo.Cities", "ID");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Companies", "CityID", "dbo.Cities");
            DropForeignKey("dbo.CompanyContacts", "Company_ID", "dbo.Companies");
            DropIndex("dbo.CompanyContacts", new[] { "Company_ID" });
            DropIndex("dbo.Companies", new[] { "CityID" });
            AlterColumn("dbo.Companies", "CityID", c => c.Int(nullable: false));
            DropTable("dbo.CompanyContacts");
            CreateIndex("dbo.Companies", "CityID");
            AddForeignKey("dbo.Companies", "CityID", "dbo.Cities", "ID", cascadeDelete: true);
        }
    }
}
