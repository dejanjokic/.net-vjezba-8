namespace Vjezba.Web.Migrations
{
    using Models.Mock;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Vjezba.Web.Models.CompaniesManagerDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Vjezba.Web.Models.CompaniesManagerDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            //
            //    context.People.AddOrUpdate(
            //      p => p.FullName,
            //      new Person { FullName = "Andrew Peters" },
            //      new Person { FullName = "Brice Lambson" },
            //      new Person { FullName = "Rowan Miller" }
            //    );
            //

            List<City> cityList = new List<City>() {
                                new City {Name = "Siverek", PostalCode = "XD9 6GR"},
                                new City {Name = "Miraj", PostalCode = "EI77 7RS"},
                                new City {Name = "Watson Lake", PostalCode = "P1 5UY"}
            };

            context.Cities.AddOrUpdate(c => c.Name, cityList[0], cityList[1], cityList[2]);
        }
    }
}
