﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Vjezba.Web.Models.Mock
{
    public enum Kontakt { Tel, Mob, Email };
    public class CompanyContact
    {
        public Kontakt kontakt { get; set; }
        [Key]
        public String value { get; set; }
    }
}