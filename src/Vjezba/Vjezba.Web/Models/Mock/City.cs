﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.ComponentModel.DataAnnotations;
using Ninject;

namespace Vjezba.Web.Models.Mock
{
    public class City
    {
        [Key]
        public int ID { get; set; }
        public string PostalCode { get; set; }
        public string Name { get; set; }
    }

}