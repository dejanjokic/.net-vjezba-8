﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Models
{
    public class CompaniesManagerDbContext : DbContext
    {
        public DbSet<Company> Companies { get; set; }
        public DbSet<City> Cities { get; set; }
        public DbSet<CompanyContact> Contacts { get; set; }
    }
}