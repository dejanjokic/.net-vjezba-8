﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Vjezba.Web.Models;
using Vjezba.Web.Models.Mock;

namespace Vjezba.Web.Controllers
{
    [RoutePrefix("komp")]
    public class CompanyController : Controller
    {
        public ActionResult Index(string query)
        {
            var dbContext = new CompaniesManagerDbContext();

            List<Company> model = dbContext.Companies.Include("City").ToList();

            dbContext.Dispose();

            return View(model);
        }

        [HttpPost]
        public ActionResult IndexAjax(CompanyFilterModel model)
        {
            var dbContext = new CompaniesManagerDbContext();

            var companiesQuery = dbContext.Companies.Include("City").ToList();

            dbContext.Dispose();

            if (!string.IsNullOrWhiteSpace(model.Name))
                companiesQuery = companiesQuery.Where(p => p.Name.Contains(model.Name)).ToList();

            if (!string.IsNullOrWhiteSpace(model.Address))
                companiesQuery = companiesQuery.Where(p => p.Address.Contains(model.Address)).ToList();

            if (!string.IsNullOrWhiteSpace(model.Email))
                companiesQuery = companiesQuery.Where(p => p.Email.Contains(model.Email)).ToList();

            if (!string.IsNullOrWhiteSpace(model.CityName))
                companiesQuery = companiesQuery.Where(p => p.City.Name.Contains(model.CityName)).ToList();

            var data = companiesQuery.OrderBy(p => p.ID).ToList();

            //Iskoristit ćemo postojeći view Index. Potrebno je eksplicitno reći koji view želimo renderirati
            //jer se inače pokušava pronaći template AdvancedSearch.cshtml
            return PartialView("_IndexTable", data);
        }

        public ActionResult Create()
        {
            this.FillDropDownValues();
            return View();
        }

        [HttpPost]
        public ActionResult Create(Company model)
        {
            if (ModelState.IsValid)
            {
 
                var dbContext = new CompaniesManagerDbContext();

                dbContext.Companies.Add(model);
                dbContext.SaveChanges();
                dbContext.Dispose();

                return RedirectToAction("Index");
            }
            else
            {
                this.FillDropDownValues();
                return View(model);
            }
        }

        public ActionResult Edit(int id)
        {
            this.FillDropDownValues();

            var dbContext = new CompaniesManagerDbContext();

            var model = dbContext.Companies.Find(id);

            dbContext.Dispose();

            return View(model);
        }

        [HttpPost]
        [ActionName("Edit")]
        public ActionResult EditPost(int id)
        {
            var dbContext = new CompaniesManagerDbContext();
            var model = dbContext.Companies.Find(id);
            var didUpdateModelSucceed = this.TryUpdateModel(model);

            if (didUpdateModelSucceed && ModelState.IsValid)
            {
                dbContext.SaveChanges();
                dbContext.Dispose();
                return RedirectToAction("Index");
            }

            dbContext.Dispose();

            this.FillDropDownValues();

            return View(model);
        }

        public ActionResult Details(int? id = null)
        {
            if (id == null)return View();

            var dbContext = new CompaniesManagerDbContext();

            var model = dbContext.Companies.Where(comp => comp.ID == id).FirstOrDefault();

            return View(model);
        }

        [HttpPost]
        public JsonResult Delete(int? id = 0)
        {
            var dbContext = new CompaniesManagerDbContext();

            if (id == 0) return Json("Failed");

            var Company = dbContext.Companies.Find(id);

            dbContext.Entry(Company).State = System.Data.Entity.EntityState.Deleted;
            dbContext.SaveChanges();

            return Json("Success");
        }


        private void FillDropDownValues()
        {
            var dbContext = new CompaniesManagerDbContext();

            var possibleCities = dbContext.Cities.ToList();

            var selectItems = new List<SelectListItem>();

            dbContext.Dispose();

            //Polje je opcionalno
            var listItem = new SelectListItem();
            listItem.Text = "- odaberite -";
            listItem.Value = "";
            selectItems.Add(listItem);

            foreach (var city in possibleCities)
            {
                listItem = new SelectListItem();
                listItem.Text = city.Name;
                listItem.Value = city.ID.ToString();
                listItem.Selected = false;
                selectItems.Add(listItem);
            }
            ViewBag.PossibleCities = selectItems;
        }

        /*
         * 
        [Route("pretraga/{q:alpha:length(2,5)}")]
        public ActionResult Search(string q)
        {
            //Kada se bude radilo s bazom podataka, ova metoda još neće "otići" u bazu po podatke
            var query = MockCompanyRepository.GetInstance().All();

            if (!string.IsNullOrWhiteSpace(q))
                query = query.Where(p => p.Name.Contains(q));

            //Kada se bude radilo s bazom podataka, tek ova metoda bi generirala ispravni SQL i izvršila upit na bazi
            //te vratila samo potrebne rezultate
            var model = query.ToList();

            return View("Index", model);
        }

        */
    }
}